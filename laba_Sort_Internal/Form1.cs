﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace laba_Sort_Internal
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Delay(int timeDelay)
        {
            int i = 0;
            var delayTimer = new System.Timers.Timer();
            delayTimer.Interval = timeDelay;
            delayTimer.AutoReset = false;
            delayTimer.Elapsed += (s, args) => i = 1;
            delayTimer.Start();
            while (i == 0) { };
        }

        private void MakeGrid(int[] array, DataGridView dgv)
        {
            dgv.RowCount = 1;
            dgv.ColumnCount = array.Length;
            for (int i = 0; i < array.Length; i++)
            {
                dgv.Rows[0].Cells[i].Value = array[i];
            }
            dgv.Refresh();
            Delay(1000);
        }

        private void CopyArray(int[] arra1, int[] arra2, int l, int r)
        {
            for (int j = l, i = 0; j <= r; j++, i++)
            {
                dataGridView1.Rows[0].Cells[i].Style.BackColor = System.Drawing.Color.White;
                arra1[i] = arra2[j];
            }
            MakeGrid(arra1, dataGridView1);
        }

        private void Sort(int[] array)
        {
            int n = array.Length, l = n, r = n;
            int[] newArray = new int[n * 2];
            MakeGrid(newArray, dataGridView2);
            dataGridView1.Rows[0].Cells[0].Style.BackColor = System.Drawing.Color.Gray;
            dataGridView1.Refresh();
            dataGridView2.Rows[0].Cells[n].Style.BackColor = System.Drawing.Color.Gray;
            newArray[n] = array[0];
            MakeGrid(newArray, dataGridView2);
            dataGridView2.Rows[0].Cells[n].Style.BackColor = System.Drawing.Color.White;
            for (int i = 1; i < n; i++)
            {
                dataGridView1.Rows[0].Cells[i].Style.BackColor = System.Drawing.Color.Gray;
                dataGridView1.Refresh();
                int j = n;
                if (array[i] <= newArray[n])
                {
                    while (j >= l && newArray[j] >= array[i])
                    {
                        j--;
                    }
                    for (int k = l; k <= j; k++)
                    {
                        newArray[k - 1] = newArray[k];
                    }
                    l--;
                }
                else
                {
                    while (j <= r && newArray[j] <= array[i])
                    {
                        j++;
                    }
                    for (int k = r; k >= j; k--)
                    {
                        newArray[k + 1] = newArray[k];
                    }
                    r++;
                }
                newArray[j] = array[i];
                dataGridView2.Rows[0].Cells[j].Style.BackColor = System.Drawing.Color.Gray;
                MakeGrid(newArray, dataGridView2);
                dataGridView2.Rows[0].Cells[j].Style.BackColor = System.Drawing.Color.White;
            }
            CopyArray(array, newArray, l, r);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int n = (int)numericUpDown1.Value;
            Random rnd = new Random();
            int[] array = new int[n];
            for (int i = 0; i < n; i++)
            {
                array[i] = rnd.Next(1, 20);
            }
            MakeGrid(array, dataGridView1);
            Sort(array);
            MessageBox.Show("Отсортировано");
        }
    }
}
